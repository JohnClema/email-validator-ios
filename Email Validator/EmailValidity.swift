//
//  EmailValidity.swift
//  Email Validator
//
//  Created by John Clema on 13/06/2016.
//  Copyright © 2016 JohnClema. All rights reserved.
//

import Foundation

class EmailValidity : NSObject {
    let email : String?
    let user : String?
    let formatValid : Bool?
    let mXFound : Bool?
    let SMTPCheck : Bool?
    let role : Bool?
    let disposable : Bool?
    let free : Bool?
    let score : Float
    
    
    
    func validityDesription() -> String {
        let validString = formatValid == true ? "has a valid format" : "has incorrect format"
        let mXFoundString = mXFound == true ? "has valid MX-Records" : "has no valid MX-Records"
        let SMTPCheckString = SMTPCheck == true ? "succeeded an SMTP check" : "failed an SMTP check"
        let freeString = free == true ? "free" : "not free"

        
        return "\(email!) \(validString), \(mXFoundString), \(SMTPCheckString) and is \(freeString)"
    }
    
    init(dictionary: [String : AnyObject]) {
        email = dictionary[MailboxLayerAPIConstants.ResponseKeys.Email] as? String
        user = dictionary[MailboxLayerAPIConstants.ResponseKeys.User] as? String
        formatValid = dictionary[MailboxLayerAPIConstants.ResponseKeys.FormatValid] as? Bool
        mXFound = dictionary[MailboxLayerAPIConstants.ResponseKeys.MXFound] as? Bool
        SMTPCheck = dictionary[MailboxLayerAPIConstants.ResponseKeys.SMTPCheck] as? Bool
        role = dictionary[MailboxLayerAPIConstants.ResponseKeys.Role] as? Bool
        disposable = dictionary[MailboxLayerAPIConstants.ResponseKeys.Disposable] as? Bool
        free = dictionary[MailboxLayerAPIConstants.ResponseKeys.Free] as? Bool
        score = dictionary[MailboxLayerAPIConstants.ResponseKeys.Score] as! Float
    }
    
    
    
}