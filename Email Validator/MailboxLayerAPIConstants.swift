//
//  Constants.swift
//  Email Validator
//
//  Consumes the API located at https://mailboxlayer.com/ 
//
//  Created by John Clema on 13/06/2016.
//  Copyright © 2016 JohnClema. All rights reserved.
//

import Foundation

struct MailboxLayerAPIConstants {
    struct QueryKeys {
        static let AccessKey = "access_key"
        static let STMP = "smtp"
        static let Email = "email"
    }
    
    struct QueryValues {
        static let AccessKey = "e9b54ac67e0c57ebcd6f385bb8085796"
        static let SMTP = 1
    }

    struct ResponseKeys {
        static let Email = "email"
        static let DidYouMean = "did_you_mean"
        static let User = "user"
        static let FormatValid = "format_valid"
        static let MXFound = "mx_found"
        static let SMTPCheck = "smtp_check"
        static let CatchAll = "catch_all"
        static let Role = "role"
        static let Disposable = "disposable"
        static let Free = "free"
        static let Score = "score"
    }
    
    struct Path {
        static let APIScheme = "https"
        static let APIHost = "apilayer.net"
        static let APIPath = "/api/check"
    }
}