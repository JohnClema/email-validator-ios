//
//  ViewController.swift
//  Email Validator
//
//  Created by John Clema on 13/06/2016.
//  Copyright © 2016 JohnClema. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var emailValidity: UILabel!
    @IBOutlet weak var validityScore: UILabel!
    
    var emailValid : EmailValidity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func mailboxLayerURLFromParameters(parameters: [String : AnyObject]) -> NSURL! {
        
        let components = NSURLComponents()
        
        components.scheme = MailboxLayerAPIConstants.Path.APIScheme
        components.host = MailboxLayerAPIConstants.Path.APIHost
        components.path = MailboxLayerAPIConstants.Path.APIPath
        components.queryItems = [NSURLQueryItem]()
        
        for (key, value) in parameters {
            let queryItem = NSURLQueryItem(name: key, value: "\(value)")
            components.queryItems!.append(queryItem)
        }
        
        
        return components.URL!
    }
    
    func validateEmail(email : String) {
        showValidUI(false)
        
        let session : NSURLSession = NSURLSession.sharedSession()
        
        let parameters : [String:AnyObject] = [
            MailboxLayerAPIConstants.QueryKeys.AccessKey:MailboxLayerAPIConstants.QueryValues.AccessKey,
            MailboxLayerAPIConstants.QueryKeys.Email:email,
            MailboxLayerAPIConstants.QueryKeys.STMP:MailboxLayerAPIConstants.QueryValues.SMTP,
        
        ]
        
        let request = NSMutableURLRequest(URL: mailboxLayerURLFromParameters(parameters))

        let task = session.dataTaskWithRequest(request) {
            (data, response, error) in
                print(data)
                print(response)
                print(error)
            
                func displayError(error: String) {
                    performUIUpdatesOnMain({ () -> Void in
//                    self.setUIEnabled(true)
//                    self.photoTitleLabel.text = "No Photo returned. Try Again."
//                    self.photoImageView.image = nil
                    })
                }
            
            guard (error == nil) else {
                displayError("No data was returned by the search!")
                return
            }
            
            guard let statusCode = (response as? NSHTTPURLResponse)?.statusCode where statusCode >= 200 && statusCode <= 299 else {
                displayError("Your request has returned a result other than 2xx!")
                return
            }
            
            let parsedResult: AnyObject!
            
            do {
                parsedResult = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            }
            catch {
                displayError("Could not parse the data as JSON: '\(data)")
                return
            }
            
            guard let valid = parsedResult[MailboxLayerAPIConstants.ResponseKeys.FormatValid] as? Bool else {
                displayError("Flickr API returned an error. See error code and message in \(parsedResult)")
                return
            }
            
            do {
                self.emailValid = EmailValidity(dictionary: parsedResult as! [String : AnyObject])
                    performUIUpdatesOnMain({ () -> Void in
                        self.emailValidity.text = self.emailValid?.validityDesription()
                        self.validityScore.text = "Validity Score: \(self.emailValid!.score)"
                        UIView.animateWithDuration(0.5, animations: {
                            self.emailValidity.alpha = 1;
                            self.validityScore.alpha = 1;
                        })
                    })
            }
            
            
            
//            self.isValidLabel.text = valid ? "Valid" : "Not Valid"
            
        }
        task.resume()
    }
    
    private func showValidUI(show: Bool) {
        if show {
            UIView.animateWithDuration(0.5, animations: {
                self.emailValidity.text = self.emailValid?.validityDesription()
                self.validityScore.text = "Validity Score: \(self.emailValid!.score)"
                self.emailValidity.alpha = 1;
                self.validityScore.alpha = 1;
            })
        } else {
            UIView.animateWithDuration(0.5, animations: {
                self.emailValidity.alpha = 0;
                self.validityScore.alpha = 0;
            })
        }
    
    }
    
}

// MARK: - ViewController (TextFieldDelegate)

extension ViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.placeholder = nil
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.placeholder = ""
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        validateEmail(textField.text!)
        textField.resignFirstResponder()
        return true
    }

}

