//
//  UIUpdatesOnMain.swift
//  Email Validator
//
//  Created by John Clema on 13/06/2016.
//  Copyright © 2016 JohnClema. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(updates: () -> Void) {
    dispatch_async(dispatch_get_main_queue()) {
        updates()
    }
}